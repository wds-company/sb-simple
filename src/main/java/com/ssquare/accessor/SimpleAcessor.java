package com.ssquare.accessor;

import java.util.Map;

import org.apache.commons.logging.Log;

import com.ssquare.common.SimpleCommon;
import com.ssquare.facade.accessor.ServiceAccessor;

public class SimpleAcessor {

	/**
	 * You method of any accessor have one parameter, it is instance of
	 * ServiceAccessor
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	public void simpleMethod(ServiceAccessor accessor) throws Exception {

		// To get logger
		Log logger = accessor.common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("simpleMethod start");

		// To do something in try block
		try {

			// To get all of request
			// RequestData request = accessor.context.request;

			// To get data part in request
			Map<String, Object> data = accessor.context.request.data;

			// To get some param in data
			String name = (String) data.get("name");
			String surname = (String) data.get("surname");

			// To call your common if you want
			SimpleCommon youCommon = new SimpleCommon();
			String fullName = youCommon.simpleCommonMethod(name, surname);
			
			// To call your common another method
			Map<String, ?> countChar = youCommon.simpleCommonMethodMap(fullName);

			// Prepare the result to return
			Map<String, Object> result = accessor.context.response.data;
			result.put("fullName", fullName);
			result.put("countChar", countChar);

		} catch (Exception e) {
			logger.error("Description of error");
			throw e;
		}

		logger.debug("simpleMethod end");

	}
}
