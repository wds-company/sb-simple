package com.ssquare.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This class to tell springBoot that it is controller package
 * @author adirak-vdi
 *
 */
@SpringBootApplication
public class MainApplication {

	/**
	 * Don't edit this code
	 * @param args
	 */
	public static void main(String[] args) {		
		SpringApplication.run(MainApplication.class, args);    
	}

}