package com.ssquare.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssquare.accessor.SimpleAcessor;
import com.ssquare.facade.accessor.ServiceAccessor;
import com.ssquare.facade.common.model.RequestData;
import com.ssquare.facade.common.model.ResponseData;

@Controller
@RequestMapping("/apis/msa")
public class SimpleController {

	/**
	 * To listUser
	 * 
	 * @param request
	 * @param requestData
	 * @return
	 */
	@RequestMapping(value = "/simpleApi", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseData simpleApi(HttpServletRequest request, @RequestBody RequestData requestData) {

		// To get logger
		Log logger = ServiceAccessor.getLogging().getLogger(this);

		logger.info("simpleApi Start");

		// Create ServiceAccessor
		ServiceAccessor serviceAcc = new ServiceAccessor(request, requestData);

		// To do in try block
		try {

			// Call custom accessor that you implement
			SimpleAcessor accessor = new SimpleAcessor();
			accessor.simpleMethod(serviceAcc);

		} catch (Exception e) {
			logger.info(e);
			serviceAcc.throwException(e);
		}

		logger.info("simpleApi End");

		// Return response data to front-end
		return serviceAcc.context.response;
	}

}
