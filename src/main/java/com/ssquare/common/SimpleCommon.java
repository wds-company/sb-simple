package com.ssquare.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.ssquare.facade.common.Common;

public class SimpleCommon extends Common {

	/**
	 * You method of any accessor have one parameter, it is instance of
	 * ServiceAccessor
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	public String simpleCommonMethod(String name, String surname) throws Exception {

		// To get logger
		Log logger = common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("simpleCommonMethod start");

		// In this class you can use all common function
		// By call common member

		// add your logic here
		String fullName = name + " " + surname;

		logger.debug("simpleCommonMethod end");

		return fullName;
	}

	/**
	 * You method of any accessor have one parameter, it is instance of
	 * ServiceAccessor
	 * 
	 * @param accessor
	 * @throws Exception
	 */
	public Map<String, ?> simpleCommonMethodMap(String fullName) throws Exception {

		// To get logger
		Log logger = common.logging.getLogger(this);

		// Stamp log to start
		logger.debug("simpleCommonMethod start");

		// In this class you can use all common function
		// By call common member

		// add your logic here
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for (int i = 0; i < fullName.length(); i++) {
			String key = fullName.charAt(i) + "";
			key = key.trim().toLowerCase();
			if (!key.isEmpty()) {
				Integer count = countMap.get(key);
				if (count != null) {
					count += 1;
				} else {
					count = 1;
				}
				countMap.put(key, count);
			}
		}

		logger.debug("simpleCommonMethod end");

		return countMap;
	}

}
